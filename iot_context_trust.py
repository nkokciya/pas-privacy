from text_processing import *
from SL_methods import *
from survey_methods import *
import pandas as pd

from sklearn.metrics import accuracy_score

def get_decision_contexts_single(clusters_rs, class_labels_probs, scenario_id, y_labels, bdu_method,update_on=0):
    """ Computing context-based privacy decisions for single context (PAS-S)
    Parameters
    ----------
    clusters_rs: dict
        The dictionary item keeping r,s values for each context
    class_labels_probs: list of tuples
        It keeps the context labels and their computed confidence scores
    scenario_id: int
        ID of the specific scenario to make the computation
    y_labels: set
        True labels provided by a user for the scenarios
    bdu_method: string
        It takes the value of "josang" or "wang" to compute BDU values
    update_on: 0 or 1
        If 1, the contexts contributing to the decision will be updated
        
    Returns
    ----------
    clusters_rs: dict
        The updated dictionary item keeping r,s values for each context (if update is on)
    decisions: set including 0 or 1
        The decision assigned for the particular scenario
    conflicts: dict
        It reports conflict values within each context
    weighted_prob: float
        The expected probability value.
    """
    decisions = []
    conflicts = {}
    
    alphas = []
    
    print(clusters_rs) 
    scenario_clusters = class_labels_probs[scenario_id]
    labels = scenario_clusters[0]
    values = scenario_clusters[1]
    
    max_conf = values[0] # maximum confidence score for the first label
    
    # computation of BDU
    bdu_clusters = {}
    if bdu_method == "wang":
        bdu_clusters = run_clusters_bdu_wang_singh(clusters_rs)
    elif bdu_method == "josang":
        bdu_clusters = run_clusters_bdu_josang(clusters_rs)
    
    print(bdu_clusters)
    
    # initial sharing decision
    dec=0
    
    dom_c = labels[0]
    alpha= abs(ALPHA_UPPER-bdu_clusters[dom_c][1])
    prob = get_prob_josang_for_bdu(bdu_clusters[dom_c][0],bdu_clusters[dom_c][2],alpha)
    print("label: C", dom_c, "single context prob: ", prob)
    
    weighted_prob = prob
    #print("avg alpha:", alphas)
    
    # the decision is share if above threshold theta
    if weighted_prob >= DEC_THETA:
        dec = 1
    decisions.append(dec)
    # print comment
    print("decision: ", decisions, "true label: ", y_labels[scenario_id], " single context prob: ", weighted_prob)
    
    return clusters_rs, decisions, conflicts, weighted_prob

def get_decision_contexts(clusters_rs, class_labels_probs, scenario_id, y_labels, bdu_method,update_on=0):
    """ Computing context-based privacy decisions for multiple contexts
    Parameters
    ----------
    clusters_rs: dict
        The dictionary item keeping r,s values for each context
    class_labels_probs: list of tuples
        It keeps the context labels and their computed confidence scores
    scenario_id: int
        ID of the specific scenario to make the computation
    y_labels: set
        True labels provided by a user for the scenarios
    bdu_method: string
        It takes the value of "josang" or "wang" to compute BDU values
    update_on: 0 or 1
        If 1, the contexts contributing to the decision will be updated
        
    Returns
    ----------
    clusters_rs: dict
        The updated dictionary item keeping r,s values for each context (if update is on)
    decisions: set including 0 or 1
        The decision assigned for the particular scenario
    conflicts: dict
        It reports conflict values within each context
    weighted_prob: float
        The expected probability value.
    """
    decisions = []
    conflicts = {}
    
    alphas = []
    
    print(clusters_rs) 
    scenario_clusters = class_labels_probs[scenario_id]
    labels = scenario_clusters[0]
    values = scenario_clusters[1]
    
    max_conf = values[0] # maximum confidence score for the first label
    
    # computation of BDU
    bdu_clusters = {}
    if bdu_method == "wang":
        bdu_clusters = run_clusters_bdu_wang_singh(clusters_rs)
    elif bdu_method == "josang":
        bdu_clusters = run_clusters_bdu_josang(clusters_rs)
    
    print(bdu_clusters)
    
    # initial sharing decision
    dec=0
    
    # computation of weighted average of context-based probabilities
    weighted_sum = 0
    weight_total = 0
    for i in range(0,len(labels)):
        alpha= abs(ALPHA_UPPER-bdu_clusters[labels[i]][1])
        alphas.append(alpha)
        prob = get_prob_josang_for_bdu(bdu_clusters[labels[i]][0],bdu_clusters[labels[i]][2],alpha)
        conf = abs(bdu_clusters[labels[i]][0]-bdu_clusters[labels[i]][1])
        conflicts[labels[i]] = conf
        weighted_sum += values[i]*prob
        weight_total += values[i]
        print("label: C", labels[i], "confidence: ", values[i], "prob: ", prob)
    
    weighted_prob = weighted_sum / weight_total
    #print("avg alpha:", alphas)
    
    # the decision is share if above threshold theta
    if weighted_prob >= DEC_THETA:
        dec = 1
    decisions.append(dec)
    # print comment
    print("decision: ", decisions, "true label: ", y_labels[scenario_id], " weighted prob: ", weighted_prob)
                    
    return clusters_rs, decisions, conflicts, weighted_prob

def update_rs_context(clusters_rs, class_labels_probs, scenario_id, y_labels, conflicts, dec, update_on=0):
    """
    Update r-s values according to decision

    Parameters
    ----------
    clusters_rs: dict
        The dictionary item keeping r,s values for each context
    class_labels_probs: list of tuples
        It keeps the context labels and their computed confidence scores
    scenario_id: int
        ID of the specific scenario to make the computation
    y_labels: set
        True labels provided by a user for the scenarios
    conflicts : set of floats
        Conflict values in each context
    dec : integer
        1 or 0 to show allow/deny data collection
    update_on: 0 or 1
        If 1, the contexts contributing to the decision will be updated
    

    Returns
    -------
    clusters_rs: dict
        The updated dictionary item keeping r,s values for each context (if update is on)

    """
    scenario_clusters = class_labels_probs[scenario_id]
    labels = scenario_clusters[0]
    values = scenario_clusters[1]
    
    max_conf = values[0]
    
    # update phase for experience-bases
    if update_on:
        # update r,s values for the involved clusters
        for i in range(0,len(labels)):
            if values[i] > max_conf-CONF_PSI: 
                # conflict value is set to 0.1 TODO
                if conflicts[labels[i]] < CONFLICT_RATIO and values[i]>0.5:
                    # no update should be done
                    break
            
            if values[i] > max_conf-CONF_PSI: 
                if y_labels[scenario_id] == dec:
                    clusters_rs[labels[i]][0] += 1 # update r value
                else:
                    clusters_rs[labels[i]][1] += 1 # update s value
    
    return clusters_rs

# input array of survey ids
def get_class_probabilities(tf_vectorizer,clf_svm):
    """
    Applies multi-label classifier to all scenarios to get class probabilities 

    Parameters
    ----------
    tf_vectorizer : vectorizer
        tf-vectorizer used to vectorize scenarios.
    clf_svm : classifier
        SVM classifier to use for classification.

    Returns
    -------
    df_all_class : dataframe
        Data frame including all scenarios with their class probabilities

    """
    from sklearn.feature_extraction.text import TfidfVectorizer
    
    df_all_class = pd.DataFrame(columns=["C0","C1","C2","C3"])
    
    df_raw = pd.read_csv('data/raw_scenarios.tsv', sep='\t')        
        
    ######## get classification labels for the scenarios in the specific survey
    for i in range(0,380):
        ex = df_raw['Text'].iloc[i]
        pre_ex = preprocess_corpus([ex])[0]
        pre_ex = return_text_scenario(pre_ex)
        pre_ex_vectorized = tf_vectorizer.fit_transform(np.array([pre_ex]))
        prob_ex=get_softmax_prob(clf_svm,pre_ex_vectorized)[0]
        # sort from big values to small values
        print(prob_ex)
        df_all_class = df_all_class.append({'C0':prob_ex[0],'C1':prob_ex[1],'C2':prob_ex[2],'C3':prob_ex[3]},ignore_index=True)
   
    df_all_class.index = range(1,381)
    
    return df_all_class
    
##################################### specific survey testing after we get labels for clusters
# setting the global parameters for the model
CONFLICT_RATIO = 0.4
CONF_PSI = 0.2
DEC_THETA= 0.5
ALPHA_UPPER = 0.6
DOMINANT_TH = 0.5
NUM_CLUSTERS = 4
EXP_BASE = 250
#######################

## get svm classifier to classify unseen policies
import pickle
#clf_svm = load('svm-classifier/svm-classifier-ext.joblib')
clf_svm = pickle.load(open("svm-classifier/svm-classifier-ext.p", 'rb'))
#
## loading the Tfidf vectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
## get tf-idf transformer
tf = pickle.load(open("svm-classifier/tfidf-ext.pkl", 'rb'))
tf_vectorizer = TfidfVectorizer(vocabulary = tf.vocabulary_)


# getting survey5 related data including participant responses
survey_id = 5
user_ids= range(1,26)

df_raw_survey_part, df_partXscen, iuipc_numeric = analyse_survey(survey_id)
df_raw_survey_specific = get_df_raw_specific_survey(survey_id)

df_raw_survey_part.index = range(1,15)
df_partXscen.index = range(1,26)
iuipc_numeric.index =  range(1,26)

######## get classification labels for the scenarios in the specific survey
scenario_no = df_raw_survey_specific.shape[0]
class_probabilities = []

leading_cases = []
leading_classes = []

devices = []
devices_byloc = []

for i in range(0,scenario_no):
    ex = df_raw_survey_specific['Text'].iloc[i]
    devices.append(df_raw_survey_specific['device_code'].iloc[i])
    devices_byloc.append(df_raw_survey_specific['device_code'].iloc[i]+"-"+df_raw_survey_specific['loc_code'].iloc[i])
    
    pre_ex = preprocess_corpus([ex])[0]
    pre_ex = return_text_scenario(pre_ex)
    pre_ex_vectorized = tf_vectorizer.fit_transform(np.array([pre_ex]))
    prob_ex=get_softmax_prob(clf_svm,pre_ex_vectorized)[0]
    # sort from big values to small values
    indices = prob_ex.argsort()[-4:][::-1]
    values = prob_ex[indices]
    class_probabilities.append((indices,values)) 
    if values[0] > DOMINANT_TH:
        leading_cases.append(1)
    else:
        leading_cases.append(0)
    leading_classes.append(indices[0])
    
### paper users
#user_ids = [1,4,3,6]  

#group 1
user_ids = [1, 10, 14, 20, 21]

#group 2
#user_ids = [3, 4, 5, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 19, 22, 25]

#group 3
#user_ids = [2, 6, 23, 24]

exp_base = EXP_BASE
up=1 # rs update is on
y_labels = []
human_agent_labels = []
pred_labels = []
pred_labels_model5 = []

all_probs_m1 = []
all_probs_m5 = []
all_probs_human = []

hum_ag_acc = []
filt_acc = []
m1_acc = []
m5_acc = []
u_inputs = []


df_results = pd.DataFrame(index=range(1,15))

import copy

# load experience bases
rs_data = pickle.load( open( "data/ijcai/250/rs.p", "rb" ) )

for u in user_ids:
    print("########################################################################")
    print("user id: "+ str(u))
    iuipc_score = iuipc_numeric.iloc[u-1]["avg"]
    
    all_probs_m1 = []
    all_probs_m5 = []
    all_probs_human = []
    user_inputs = [0] * 14
    
    clusters_rs, devices_rs, devices_byloc_rs = rs_data[u][0], rs_data[u][1], rs_data[u][2]
    
    clusters_rs_m3 = copy.deepcopy(clusters_rs) 
    clusters_rs_m5 = copy.deepcopy(clusters_rs) 
    devices_rs_m3 = copy.deepcopy(devices_rs)
    
    y_labels.extend(df_partXscen.iloc[u-1])
       
    filtered_dec = []
    filtered_truth = []
    u_dec = []
    conf_counter = 0
    for i in range(0,scenario_no):
        print("scenario id: "+ str(i))
        
        # PAS-M
        print('PAS-M:')
        clusters_rs, d, conflicts, prob_m1 = get_decision_contexts(clusters_rs, class_probabilities, i, y_labels, "wang",update_on=up)
        pred_labels.append(d[0])
        
        
        # PAS-S
        print('PAS-S:')
        clusters_rs_m5, d5, conflicts_dummy, prob_m5 = get_decision_contexts_single(clusters_rs_m5, class_probabilities, i, y_labels, "wang",update_on=up)
        pred_labels_model5.append(d5[0])
        
        # PAS
        print('PAS:')
        if leading_cases[i] == 0 or (leading_cases[i] == 1 and conflicts[leading_classes[i]]>CONFLICT_RATIO):
            filtered_dec.append(d[0])
            filtered_truth.append(y_labels[i])
            human_agent_labels.append(d[0])
            print('PAS makes an automated decision, same as PAS-M decision.')
        else:
            print("PAS asks user.")
            print("Conflict ratio: "+ str(conflicts[leading_classes[i]])+ " Dominant context: "+ str(leading_classes[i]))
            conf_counter = conf_counter + 1
            user_inputs[i]=1
            human_agent_labels.append(y_labels[i])
            d[0] = y_labels[i]
        print("\n")
        
        # update r-s context PAS-M
        update_rs_context(clusters_rs, class_probabilities, i, y_labels, conflicts, d[0], update_on=up)
    
    
    acc_score = accuracy_score(filtered_truth, filtered_dec) 
    overall_acc = accuracy_score(pred_labels, y_labels) 
    model5_acc = accuracy_score(pred_labels_model5, y_labels)
    ha_acc = accuracy_score(human_agent_labels, y_labels)
    
    # add accuracy values for all users
    m1_acc.append(overall_acc)
    m5_acc.append(model5_acc)
    hum_ag_acc.append(ha_acc)
    u_inputs.append(np.sum(user_inputs))
    
    
    # reporting accuracy results for the cases the agent makes a decision
    print("total user inputs: ", np.sum(user_inputs))
    print("PAS accuracy: ", ha_acc)
    #print("filtered accuracy: ", acc_score)
    print("PAS-M accuracy: ", overall_acc)
    print("PAS-S accuracy: ", model5_acc)
    
    y_labels = []
    pred_labels = []
    pred_labels_model5 = []
    human_agent_labels = []
    
print("\nall users avg values:"   )
print("avg user inputs: ", np.mean(u_inputs))
print("PAS average accuracy: ", np.mean(hum_ag_acc))
print("PAS-M average accuracy: ", np.mean(m1_acc))
print("PAS-S average accuracy: ", np.mean(m5_acc))

def grouping_people_by_behavior(df_partXscen,outliers):
    # 1: 66>unconcerned, 2: 33-66 pragmatist, 3: fundamentalists otherwise
    sum_per_part = pd.DataFrame()
    sum_per_part["sum"] = df_partXscen.sum(axis=1)
    sum_per_part["sharing"] = sum_per_part * 100 / 14
    
    sum_per_part["group"] = 0
    sum_per_part.loc[sum_per_part[sum_per_part["sharing"]>66].index,"group"]=1
    #sum_per_part.loc[(sum_per_part["sharing"]<66][sum_per_part["sharing"]>=33]).index,"group"]=2
    #sum_per_part.loc[sum_per_part[sum_per_part["sharing"]<66][sum_per_part["sharing"]>=33].index,"group"]=2
    sum_per_part.loc[sum_per_part[sum_per_part["sharing"]<33].index,"group"]=3
    sum_per_part.loc[sum_per_part[sum_per_part["group"]==0].index,"group"]=2
    
    return sum_per_part.loc[~sum_per_part.index.isin(outliers)]
    
    
