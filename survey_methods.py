import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer

from text_processing import init_clusters,preprocess_corpus, return_text_scenario

from scipy.special import softmax
def get_softmax_prob(clf,scenario_vectorized):
    """
    Parameters
    ----------
    clf: 
        Classifier object
    scenario_vectorized : set
        The tokenized version of a scenario
        
    Returns
    ----------
    The probabilities for the scenario to belong to different contexts
    
    """
    p = np.array(clf.decision_function(scenario_vectorized))
    return softmax(p, axis=1)

def analyse_survey(survey_id):
    """
    Parameters
    ----------
    survey_id: integer
        The specific survey to analyze to get the responses of the participants
        and compute their average IUIPC scores.
        
    Returns
    ----------
    Three dataframes including scenarios with their features, the participants' decisions
    for each scenario, and the participants' IUIPC scores.    
    """
    df_survey = pd.read_csv('data/surveys/survey '+str(survey_id)+'.csv')
    df_survey.columns.get_loc("If you had the choice, would you allow or deny this data collection? ")
    allow_deny_cols = [df_survey.columns.get_loc(col) for col in df_survey.columns if 'If you had the choice, would you allow or deny this data collection? ' in col]
    df_survey_ad = df_survey.iloc[:,allow_deny_cols]
    df_survey_ad.columns = range(1,15) # for each scenario, we have 14 scenarios
    
    # a list of separate scenarios                   
    scenarios = list()
    i=1
    
    for col in allow_deny_cols:
        scenarios.append(df_survey.iloc[:,range(i,col+1)])
        i = col+1
    
    scenXpart = dict()    
    i=1
    for dfs in scenarios:
        scenXpart[i] = dfs.iloc[:,dfs.shape[1]-1].replace(["Allow","Deny"],[1,0])
        i = i+1
        
    df_partXscen=pd.DataFrame(scenXpart)
    # indexing by participant id
    #print "total pid: "+str(df_partXscen.shape[0]+1)
    df_partXscen.index =  range(1,df_partXscen.shape[0]+1)
    
    # creating a dataframe for IUIPC questionnaire
    q1_index =  [df_survey.columns.get_loc(col) for col in df_survey.columns if 'Consumer online privacy' in col]
    iuipc = df_survey.iloc[:,range(q1_index[0],q1_index[0]+10)]
    iuipc_numeric = iuipc.replace(["Strongly agree","Agree","Somewhat agree",
                                   "Neither agree nor disagree","Somewhat disagree",
                                   "Disagree","Strongly disagree"],range(7,0,-1))
    iuipc_numeric.columns = ["q1","q2","q3","q4","q5",
                             "q6","q7","q8","q9","q10"]
    
    # adding new metric computations according to IUIPC scale    
    iuipc_numeric["control"] = iuipc_numeric.loc[:,"q1":"q3"].mean(axis=1) 
    iuipc_numeric["awareness"] = iuipc_numeric.loc[:,"q4":"q6"].mean(axis=1) 
    iuipc_numeric["collection"] = iuipc_numeric.loc[:,"q7":"q10"].mean(axis=1) 
    iuipc_numeric["avg"] = iuipc_numeric.mean(axis=1) 
    iuipc_numeric.index =  range(1,df_partXscen.shape[0]+1) 
    
    mappings = pd.read_csv('data/scenario-mappings.csv', sep='\t')   
    mappings.index =  range(1,mappings.shape[0]+1) 
    ## reading raw scenarios ID mapping
    survey_mappings = mappings.loc[survey_id,:] 
    
    # read tab seperated scenarios
    df_raw = pd.read_csv('data/raw_scenarios.tsv', sep='\t')
    df_raw_survey = df_raw[df_raw["ID"].isin(survey_mappings)]
    
    # adding participant-based allow/deny decisions -- 
    df_raw_survey_part = df_raw_survey.loc[:,:"inferred"] # remove text
    #df_raw_s1_part.insert(0, "SID", range(1,df_raw_s1_part.shape[0]+1), True) 
    # indexing by scenario id
    df_raw_survey_part.index = range(1,df_raw_survey_part.shape[0]+1)
    
    return df_raw_survey_part, df_partXscen, iuipc_numeric

def analyse_all_surveys(rm_survey_id=5):
    """
    Parameters
    ----------
    rm_survey_id : integer, optional
        The survey to be removed from the analysis. The default is 5.

    Returns
    -------
    df_all_global : dataframe
        dataframe containing all surveys.
    df_stats : dataframe
        Allow/Deny distributions along all surveys.

    """
    
    df_all_global = pd.DataFrame(columns=range(1,383))
    df_all_global.rename(columns={381:'pid'}, inplace=True)
    df_all_global.rename(columns={382:'iuipc_avg'}, inplace=True)
    
    all_survey_ids = range(1,40)
    all_survey_ids.remove(8) # does not exist
    all_survey_ids.remove(17) # there is an error to retrieve last scenario
    
    #all_survey_ids.remove(rm_survey_id) # rm specific survey
    
    #all_survey_ids = [5,7]
    #survey specific iteration
    for surveyid in all_survey_ids:
        a, partXscen, partXiuipc = analyse_survey(surveyid)
        survey_size = len(partXscen)
        
        survey_scenarios = a["ID"].tolist()
        partXscen.columns = survey_scenarios
        partXscen["pid"] = str(surveyid)+"."+partXscen.index.astype(str)
        partXscen["iuipc_avg"] = partXiuipc["avg"] 
        survey_scenarios.append("pid")
        survey_scenarios.append("iuipc_avg")
        
        df_all_part = pd.DataFrame(data=-1,index=range(1,survey_size+1),columns=range(1,383))
        df_all_part.rename(columns={381:'pid'}, inplace=True)
        df_all_part.rename(columns={382:'iuipc_avg'}, inplace=True)
        
        df_all_part[survey_scenarios] = partXscen
        
        # append to global
        df_all_global = df_all_global.append(df_all_part,ignore_index=True)
    
    df_all_global[1:380] = df_all_global[1:380].apply(pd.to_numeric)
    
    ### allow/deny distribution
    # as stack
    global_stats = df_all_global.apply(lambda x: x.value_counts()).T.stack()
    df_stats = pd.DataFrame(data=0,index=range(1,381), columns=["Allow","Deny"])
    
    for i in range(1,381):
        if 1 in global_stats[i].keys(): 
            df_stats.loc[i,"Allow"] = global_stats[i][1]
        if 0 in global_stats[i].keys():     
            df_stats.loc[i,"Deny"] = global_stats[i][0]
    
    df_stats["Total"] = df_stats["Allow"] + df_stats["Deny"]
    df_stats["AllowP"] = df_stats["Allow"] / df_stats["Total"] *100
    df_stats["DenyP"] = df_stats["Deny"] / df_stats["Total"] *100
    
    #df_stats.loc[1:50,:].plot.bar(figsize=(20,5))
    #df_stats.loc[1:50,:].plot.bar(figsize=(20,5),y=["AllowP","DenyP"])
    
    return df_all_global, df_stats
    
    

### collect rs values from other surveys k: label number to consider
def return_clusters_rs(rm_survey_id,seed_user_id,avg_th,classification_model,tf_vectorizer,cluster_no,window,exp_base_total,bysim=1,random_seed=-1):
    """
    Parameters
    ----------
    rm_survey_id: integer
        The specific survey to analyze
    seed_user_id: integer
        ID of the user to be analyzed
    avg_th: float
        The average IUIPC privacy score of the seed user
    classification_model:
        The classifier to be used
    cluster_no: integer
        The number of clusters in the set of contexts
    window: integer
        The similarity window for the user to check for similar users
    exp_base_total: integer
        The number of experiences to include in an experience base
    bysim: integer
        1 or 0. When 1, IUIPC similarity is used.
    random_seed: number
        Randomization is based on specific seeds. Ensures same results.
        
    Returns
    ----------
    clusters_rs: dict
        The dictionary of context together with their experience bases
    devices_rs: dict
        The dictionary of context together with their experience bases based on devices
    devices_byloc_rs: dict
        The dictionary of context together with their experience bases based on devices/location
    """
    
    #import random
    clusters_rs = init_clusters(cluster_no,as_dict=0,for_classification=1)
    devices_rs = init_devices_rs()
    devices_byloc_rs =  init_devices_byloc_rs()
    
    # get survey/scenario mappings
    mappings = pd.read_csv('data/scenario-mappings.csv', sep='\t')   
    mappings.index =  range(1,mappings.shape[0]+1)
    
    
    all_survey_ids = list(range(1,40))
    all_survey_ids.remove(8) # does not exist
    all_survey_ids.remove(17) # there is an error to retrieve last scenario
    
    all_survey_ids.remove(rm_survey_id) # rm specific survey
    #print all_survey_ids
    
    # processing of surveys will be done in randomized order
    if random_seed != -1:
        import random
        
        random.seed(random_seed)
        all_survey_ids = random.sample(all_survey_ids,len(all_survey_ids))
    
    up_limit = min(7,avg_th+window)
    exp_no = 0
    
    for sid in all_survey_ids:
        df_raw_survey_part, df_partXscen, iuipc_numeric = analyse_survey(sid)
        user_ids = []
        
        if bysim==1:
            # get similar user ids according to iuipc
            user_ids = iuipc_numeric["avg"][(iuipc_numeric["avg"]>=avg_th) & (iuipc_numeric["avg"]<=up_limit)].index
            #print "bysim: ", user_ids
        else:
            # consider all users in the survey
            user_ids = iuipc_numeric.index
            #print "nosim: ", user_ids
            
        if len(user_ids) == 0:
            continue
        
        similar_users_partXscen = df_partXscen.loc[user_ids,:]
        
        allow_dec_count = similar_users_partXscen.sum()
        temp_array = np.full((1, 14), len(user_ids))[0]
        deny_dec_count = temp_array - allow_dec_count
        
        majority_voting = allow_dec_count > deny_dec_count
        
        survey_computed_decisions = []
        
        for i in range(1,15):
            dec = 0
            if majority_voting[i] == True:
                dec = 1
            survey_computed_decisions.append(dec)
            
        # get survey specific scenarios
        survey_mappings = mappings.loc[sid,:] 
        #print survey_mappings

        df_raw = pd.read_csv('data/raw_scenarios.tsv', sep='\t')
        #print df_raw
        df_raw_sid = df_raw[df_raw["ID"].isin(survey_mappings)]
        #print df_raw_sid
        
        # get cluster labels phase
        for i in range(0,14):
            ex = df_raw_sid['Text'].iloc[i]
            device = df_raw_sid['device_code'].iloc[i]
            location = df_raw_sid['loc_code'].iloc[i]
            dev_byloc = device+"-"+location
            
            #print ex
            pre_ex = preprocess_corpus([ex])[0]
            pre_ex = return_text_scenario(pre_ex)
            
            pre_ex_vectorized = tf_vectorizer.fit_transform(np.array([pre_ex]))
            prob_ex=get_softmax_prob(classification_model,pre_ex_vectorized)[0]
            # sort from big values to small values
            indices = prob_ex.argsort()[-4:][::-1]
            values = prob_ex[indices]
            
            #print prob_ex
            
            # get top 1 label
            pred_label_i = indices[0]
            
            # get high predictions, first label prediction confidence should be more than 0.5
            if values[0] < 0.5:
                continue
            
            if survey_computed_decisions[i] == 1: #positive experience
                clusters_rs[pred_label_i][0] += 1
                devices_rs[device][0] += 1
                devices_byloc_rs[dev_byloc][0] += 1
            else: # negative experience
                clusters_rs[pred_label_i][1] += 1
                devices_rs[device][1] += 1
                devices_byloc_rs[dev_byloc][1] += 1
                
            exp_no +=1        
            if exp_no == exp_base_total: # experience limit reached
                return clusters_rs, devices_rs, devices_byloc_rs
    
    return clusters_rs, devices_rs, devices_byloc_rs

def return_clusters_randomized(rm_survey_id,seed_user_id,avg_th,classification_model,tf_vectorizer,cluster_no,window,exp_base_total,bysim=1,simple=0):
    """
    Computes experience-based based on average of ten iterations.

    """
    clusters_rs_avg = init_clusters(cluster_no,as_dict=0,for_classification=1)
    devices_rs_avg = init_devices_rs()
    devices_byloc_rs_avg =  init_devices_byloc_rs()
    
    roundno = 10
    
    for i in range(0,roundno):
        # random seed is set as i
        if simple == 0:
            c, d = return_clusters_rs_proportion(rm_survey_id,seed_user_id,avg_th,classification_model,tf_vectorizer,cluster_no,window,exp_base_total,bysim=1, random_seed=i)
        else:
            c, d, dloc = return_clusters_rs(rm_survey_id,seed_user_id,avg_th,classification_model,tf_vectorizer,cluster_no,window,exp_base_total,bysim=1, random_seed=i)
        
        for cid in c.keys():
            clusters_rs_avg[cid][0] += c[cid][0]*1.0/roundno
            clusters_rs_avg[cid][1] += c[cid][1]*1.0/roundno 
            
        for did in d.keys():
            devices_rs_avg[did][0] += d[did][0]*1.0/roundno
            devices_rs_avg[did][1] += d[did][1]*1.0/roundno
            
        for did_loc in dloc.keys():
            devices_byloc_rs_avg[did_loc][0] += dloc[did_loc][0]*1.0/roundno
            devices_byloc_rs_avg[did_loc][1] += dloc[did_loc][1]*1.0/roundno
            
    return clusters_rs_avg, devices_rs_avg, devices_byloc_rs_avg

def get_df_raw_specific_survey(survey_id):
    """
    Parameters
    ----------
    survey_id: integer
        The specific survey to analyze
    
    Returns
    ----------
    a dataframe that includes all features of the scenarios together with
    the textual scenario
    """
    
    mappings = pd.read_csv('data/scenario-mappings.csv', sep='\t')   
    mappings.index =  range(1,mappings.shape[0]+1)  
    ## reading raw scenarios ID mapping
    survey_mappings = mappings.loc[survey_id,:] 

    df_raw = pd.read_csv('data/raw_scenarios.tsv', sep='\t')
    df_raw_survey_specific = df_raw[df_raw["ID"].isin(survey_mappings)]
    
    return df_raw_survey_specific