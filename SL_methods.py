"""
This file includes methods to compute BDU values and their corresponding 
probabilities according to Josang method or Wang and Singh method.
"""

def return_josang_bdu(r,s):
    denom_rs = r + s + 2  
    b = r*1.0 / denom_rs
    d = s*1.0 / denom_rs
    u = 2.0 / denom_rs
    
    return [b,d,u]
        
from scipy.integrate import quad
def bdu_denom(x, r, s):
    return x**r*(1-x)**s

def first_integ(r,s):
    return quad(bdu_denom, 0, 1, args=(r,s))
    
def bdu_abs(x, r, s):
    int1 = quad(bdu_denom, 0, 1, args=(r,s))[0]
    return abs((x**r*(1-x)**s / int1)-1)

def return_wang_singh_bdu(r,s):
    c_rs = 0.5 * quad(bdu_abs, 0, 1, args=(r,s))[0]
    alpha_prob = (r+1)*1.0 / (r+s+2)
    
    u_rs = 1.0 - c_rs
    b_rs = alpha_prob * c_rs
    d_rs = (1-alpha_prob) * c_rs
    
    return [b_rs, d_rs, u_rs]

def run_clusters_bdu_josang(clusters_rs):
    clusters_bdu = {}
    
    for cid, rs in clusters_rs.items():
       clusters_bdu[cid] = return_josang_bdu(rs[0],rs[1])

    return clusters_bdu

def run_clusters_bdu_wang_singh(clusters_rs):
    clusters_bdu = {}
    
    for cid, rs in clusters_rs.items():
        clusters_bdu[cid] = return_wang_singh_bdu(rs[0],rs[1])

    return clusters_bdu 

def get_prob_josang(clusters_bdu,amb=0.5):
    clusters_prob = {}
    
    for cid, rs in clusters_bdu.items():
        clusters_prob[cid] = clusters_bdu[cid][0] + amb*clusters_bdu[cid][2] 

    return clusters_prob

def get_prob_josang_for_bdu(b,u,amb=0.5):
    return b + amb*u

def get_prob_wang(clusters_bdu):
    clusters_prob = {}
    
    #print clusters_bdu
    
    for cid, rs in clusters_bdu.items():
        # TODO not the case in wang, singh paper
        if clusters_bdu[cid][0] == 0 and clusters_bdu[cid][1] == 0:
            clusters_prob[cid] = 0
            continue
        clusters_prob[cid] = clusters_bdu[cid][0] / (clusters_bdu[cid][0]+clusters_bdu[cid][1]) 
        #print clusters_prob[cid]

    return clusters_prob