# Taking Situation-Based Privacy Decisions: Privacy Assistants Working with Humans
This implementation is part of our paper "Taking Situation-Based Privacy Decisions: Privacy Assistants Working with Humans", N. Kokciyan, P. Yolum, 31st International Joint Conference on Artificial Intelligence and the 25th European Conference on Artificial Intelligence (IJCAI-ECAI 2022). **[accepted]**

**Abstract:** Privacy on the Web is typically managed by giving consent to individual Websites for various aspects of data usage. This paradigm requires too much human effort and thus is impractical for Internet of Things (IoT) applications where humans interact with many new devices on a daily basis. Ideally, software privacy assistants can help by making privacy decisions in different situations on behalf of the users. To realize this, we propose an agent-based model for a privacy assistant. The model identifies the contexts that a situation implies and computes the trustworthiness of these contexts. Contrary to traditional trust models that capture trust in an entity by observing large number of interactions, our proposed model can assess the trustworthiness even if the user has not interacted with the particular device before. Moreover, our model can decide which situations are inherently ambiguous and thus can request the human to make the decision. We evaluate various aspects of the model using a real-life data set and report adjustments that are needed to serve different types of users well. 

**Code:** We provide the code to analyze a specific survey (survey number 5) to reproduce the results reported in our paper. 

Project icon downloaded from [icon source](https://www.pngrepo.com/svg/68908/hand-pass).

# Requirements
We used Python 3.8.8 to run our experiments. Please check _requirements.txt_
for further dependencies.

# Running the code
_iot_context_trust.py_ is the main file to work with. You should change the value
of `user_ids` variable. 

By default, the conflict ratio is set to 0.4 (line 247). You can change this
value to 0.1, 0.2, 0.3 and 0.4 to get reported results in the paper.

# Setup of User IDs

For specific users reported in the supplementary material.
user_ids = [1,4,3,6]  

For group 1:
user_ids = [1, 10, 14, 20, 21]

For group 2:
user_ids = [3, 4, 5, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 19, 22, 25]

For group 3:
user_ids = [2, 6, 23, 24]

Once you set this variable, open a terminal and run the following:
```Python
python iot_context_trust.py > output.txt
```

The `output.txt` file will include the reasoning process step-by-step.

# Paper Results
In _paper-results-outputs_ folder, we provide all the outputs that you should get
once you run the code.

# Further Details
* _clusters_ folder includes four different pictures to visually described
the obtained clusters.

* _data_ folder includes the experience bases data and the survey data used in
the experiments.

# Experiment!!
Feel free to change any of the parameters to experiment with the model. Each time
results for three different agents (PAS, PAS-S, PAS-M) will be reported.
