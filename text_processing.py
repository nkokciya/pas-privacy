import numpy as np
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, WordNetLemmatizer
import string
from nltk import word_tokenize
import nltk

"""
    This file includes standard methods to preprocess data before tokenizing it.
    The stopwords are removed, stemming and lemmatization techniques are applied.
"""

def remove_duplicates_keep_order(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]

def preprocess_scenario(scenario):
    stop_words = stopwords.words('english')
    other_words = ["tthe","ffor","eg"]
    stop_words.extend(other_words)    
    
    #stemmer = LancasterStemmer()
    stemmer = PorterStemmer()
    lemmatizer = WordNetLemmatizer()
    
    text = scenario.lower()
    
    # fixing typo
    text.replace("phoon","phone")
    
    # remove punctutation
    text = "".join([char for char in text if char not in string.punctuation])
    
    tokens = word_tokenize(text)
    
    # remove stopwords
    tokens = [word for word in tokens if word not in stop_words]
    
    # stemming
    tokens = [stemmer.stem(word) for word in tokens]
    
    # lemmatize verbs
    tokens = [lemmatizer.lemmatize(word, pos='v') for word in tokens]
    
    # remove duplicate words
    #tokens = list(set(tokens))
    tokens = remove_duplicates_keep_order(tokens)
    
    return tokens

def preprocess_corpus(scenarios):

    tokenized_sent = []
    
    for s in scenarios:
        tokenized_sent.append(preprocess_scenario(s))
        
    return tokenized_sent

def sent_vectorizer(sent, model):
    sent_vec =[]
    numw = 0
    for w in sent:
        try:
            if numw == 0:
                sent_vec = model[w]
            else:
                sent_vec = np.add(sent_vec, model[w])
            numw+=1
        except:
            pass
     
    return np.asarray(sent_vec) / numw

def getFrequencyDictForCluster(cluster):
    return nltk.FreqDist(cluster)

def tokenized_corpus2text(corpus):
    text=""
    for scenario in corpus:
        for word in scenario:
            text=text+ " " +word
    
    f = open("data/tokenized-scenarios.txt", "w")
    f.write(text)
    f.close()

def return_text_scenario(scenario):
    text=''
    for word in scenario:
            text+=word+" "
    text = text[:-1]
    return text

def init_clusters(num_clusters,as_dict=0,for_classification=0):
    res ={}
    
    for i in range(0,num_clusters):
        if as_dict:
            res[i] = {}
        else:
            if for_classification:
                res[i] = [0,0]
            else:
                res[i] = []
        
    return res

def create_wordcloud_for_clusters(clusters):
    """
        creating wordclouds for each cluster/context
    """
    from wordcloud import WordCloud
    import matplotlib.pyplot as plt 
    
    clusters_word_frequencies = init_clusters(len(clusters),as_dict=1)
    
    for cid in range(0,len(clusters)):
        wordcloud = WordCloud(width = 1000, height = 1000, 
                background_color ='white',
                min_font_size = 14)
        freq = getFrequencyDictForCluster(clusters[cid])
        clusters_word_frequencies[cid]=freq
        #print cid
        #print freq.most_common()
        wordcloud.generate_from_frequencies(freq) 
        wordcloud.to_file("clusters/cluster"+str(cid)+".png")
    
        plt.figure(figsize = (10, 10), facecolor = None) 
        plt.imshow(wordcloud) 
        plt.close()
